var GameLayer = cc.Layer.extend({
    weaponSet: [],
    enemySet: [],
    skill: null,
    ctor: function(){
        this._super();
        this.init();
    },

    init: function() {
         _this = this;

        this.character = new Naruto( Math.random()*1500 , 500 );
        this.addChild( this.character );

        this.createBlocks();

        this.boss = new Boss( this.blocks );
        this.boss.setCharacter( this.character );
        this.boss.scheduleUpdate();
        this.addChild( this.boss , 2);
        this.enemySet.push( this.boss );

        this.character.setBlock( this.blocks );
        this.character.setEnemy( this.enemySet );
        this.character.scheduleUpdate();

        this.setKeyboardEnabled( true );
        this.scheduleUpdate();

        return true;
    },

    createWepon: function(){
        var weapon = new Weapon( this.randInt( 1, 4), this.blocks );
        this.addChild( weapon , 3);
    },

    createEnemy: function(){
        var ene = new EnemyClass1( this.blocks );
        ene.setCharacter( this.character );
        ene.scheduleUpdate();

        this.addChild( ene );
        this.enemySet.push( ene );
    },

    update: function(){
        var n =(-1)*( this.enemySet.length + 1 );
        var power = Math.pow( 1.71828, n);
        if( this.randInt( 10000, 0) > 10000-((1000)*power) ){
            this.createEnemy();
        }
        if( this.randInt( 10000, 0) > 9990 ){
            //this.createWepon();
        }
        if( this.character._damageNew != 0 ){
            this.addChild( new Damage( this.character._damageNew, this.character.x, this.character.y ));
            this.character._damageNew = 0;
        } 
        this.enemySet.forEach( function( e ) {
            if( e._damageNew != 0 ){
                this.addChild( new Damage( e._damageNew, e.x, e.y ));
                e._damageNew = 0;
            } 
        }, this );

        //Check Skill
        this.skill = null;
        if( this.character.STATUS == Naruto.STATUS.USESKILL1 ){            
            this.skill = new SkillNaruto( 1, this.character.isFlippedX(), this.character.x, this.character.y + 25 );
        }else if( this.character.STATUS == Naruto.STATUS.USESKILL2 && !this.character.useSkill ){     
            this.skill = new SkillNaruto( 2, this.character.isFlippedX(), this.character.x, this.character.y + 25);
            this.character.useSkill = true;
        }
        if( this.skill ){
            if( this.skill._status == "normal" ){
                this.skill.setEnemy( this.enemySet );
                this.weaponSet.push( this.skill );
                this.addChild( this.skill,3,"Skill2" );
            }
        }
        if( this.character.useSkill  ) {
            var skill = this.getChildByTag("Skill2");
            if( skill ){
                if( skill._status == "normal"){
                    this.character.x = skill.x;
                    this.character.y = skill.y;
                    this.character.ground = null;
                    this.character.updatePosition();
                }
            }else{
                this.character.useSkill = false;
                this.character.setOpacity(255);
            }
        }
    },

    randInt: function( Max, Min){
        return Math.round(Min + (Math.random() * (Max - Min)));
    },

    canBlock: function( character, player){
        var p = player.getBoundingBox();
        var charThis = character.getBoundingBox();
        if( character.isFlippedX() ){            
            var rectAtk = cc.rect(charThis.x - 5, charThis.y, (charThis.width/2) - 17, charThis.height);    
        }else{
            var rectAtk = cc.rect(charThis.x + charThis.width, charThis.y, (charThis.width/2) - 22, charThis.height);    
        }
        if( player.isFlippedX() ){   
            var rectBlock = cc.rect(p.x - 5 , p.y, (p.width/2), p.height);
        }else{
            var rectBlock = cc.rect(p.x + p.width, p.y, (p.width/2) - 5, p.height);
        }
        return cc.rectIntersectsRect( rectAtk , rectBlock );
    },

    checkAttack: function( character, player ){
        var charThis = character.getBoundingBox();
        if( character.isFlippedX() ){            
            var rectAtk = cc.rect(charThis.x - 5, charThis.y, (charThis.width/2) - 17, charThis.height);    
        }else{
            var rectAtk = cc.rect(charThis.x + charThis.width, charThis.y, (charThis.width/2) - 22, charThis.height);    
        }
        return cc.rectIntersectsRect( player.getBoundingBox() , rectAtk );
        
    },

    createBlocks: function() {
        this.blocks = [];
        var groundBlock = new Block( 0, 0, 1559, 40 );
        this.blocks.push( groundBlock );

        var building1 = new Block( 80, 190, 220, 210 );
        this.blocks.push( building1 );

        var building2 = new Block( 290, 210, 420, 250 );
        this.blocks.push( building2 );

        var building3 = new Block( 510, 250, 630, 280 );
        this.blocks.push( building3 );

        var building3_2 = new Block( 510, 140, 630, 175 );
        this.blocks.push( building3_2 );

        var building4 = new Block( 680, 180, 1150, 220 );
        this.blocks.push( building4 );

        var building5 = new Block( 1280, 180, 1350, 216 );
        this.blocks.push( building5 );

        var building5_2 = new Block( 1280, 290, 1350, 300 );
        this.blocks.push( building5_2 );

        var building6 = new Block( 1420, 190, 1550, 230 );
        this.blocks.push( building6 );

        this.blocks.forEach( function( b ) {
            this.addChild( b );
        }, this );
    },

    createWeapon: function( weapon ){
        weapon.start();
        this.addChild( weapon );
    },

    onKeyDown: function( e ) {
        this.character.handleKeyDown( e );
        //Attack Weapon
        var weapon = null;
        if( this.character._Attack3 && this.character.health > 0 && !this.character._release && this.character.delayWeapon[this.character.weapon-1] == this.character.maxDelayWepon[this.character.weapon-1]){            
            weapon = new Weapon( this.character.weapon , this.character.isFlippedX(), this.character.x, this.character.y + 25 );
        }         
        if( weapon ){
            weapon.setEnemy( this.enemySet );
            this.weaponSet.push( weapon );
            this.addChild( weapon );
        }
        //Swap weapon
        if( e == cc.KEY.up || e == cc.KEY.down){
            var f = this.getParent().getChildByTag(TagOfLayer.EffectLayer);
            f.onKeyDown(e);
        }
    },

    onKeyUp: function( e ) {
        this.character.handleKeyUp( e );
        if( e == cc.KEY.up || e == cc.KEY.down){
            var f = this.getParent().getChildByTag(TagOfLayer.EffectLayer);
            f.onKeyUp(e);
        }
    },
});

