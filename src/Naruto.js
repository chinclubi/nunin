Naruto = cc.Sprite.extend({
    _moveLeft: false,
    _moveRight: false,
    _jump: false,
    _jump2: false,
    _Attack1: false,
    _Attack2: false,
    _Attack3: false,
    _Attack4: false,
    delayStatus: 0,
    weapon: 1,
    maxDelayWepon: [],
    delayWeapon:[], // Frame
    _release: false,
    _damageNew: 0,
    _enemySet:[],
    mana: 1000,
    immortal: false,
    useSkill: false,
    ctor: function( x , y ) {
        this._super();
        this.setAnchorPoint( cc.p( 0.5, 0 ) );
        this.lastX = x;
        this.x = x;
        this.y = y;

        this.maxVx = 4;
        this.accX = 0.5;
        this.backAccX = 0.75;
        this.jumpV = 10;
        this.g = -0.5;
        
        this.vx = 0;
        this.vy = 0;

        this.ground = null;

        this.blocks = [];

        this.health = 1000;

        this.updatePosition();
        this.createAnimation();

        this.STATUS = Naruto.STATUS.STAND;
        this.lastSTATUS = null;
        this.canAttack = true;

        this.delayWeapon = [80,100,190,300];
        this.maxDelayWepon = [80,100,190,300];
        // this.createHealthBar();

        // ------- Test-------------
        // var box = new cc.Sprite();
        // var thisRect = this.getBoundingBox();
        // box.setColor(cc.GREEN);
        // box.setTextureRect( cc.rect( thisRect.x, thisRect.y, thisRect.width, thisRect.height));
        // box.setOpacity(100);
        // this.addChild(box,10,"BOX");
        // ------- Test-------------
    },

    createAnimation: function(){
        this.Action = new Array();
        this.Action[0] = this.charAnimation( "Dead" , 5 );
        this.Action[1] = this.charAnimation( "Stand" , 6 );
        this.Action[2] = this.charAnimation( "Run" , 6 );
        this.Action[3] = this.charAnimation( "Run" , 6 );
        this.Action[4] = this.charAnimation( "JumpUp" , 2 );
        this.Action[5] = this.charAnimation( "JumpDown" , 2 );
        this.Action[6] = this.charAnimation( "Attack" , 3 );
        this.Action[7] = this.charAnimation( "Throw" , 5 );
        this.Action[8] = this.charAnimation( "ThrowAir" , 3 );
        this.Action[9] = this.charAnimation( "Skill1_", 23);
        this.Action[10] = this.charAnimation( "Skill2_", 20);
        this.Action[11] = this.charAnimation( "uSkill1_", 3);
        this.Action[12] = this.charAnimation( "Skill2_", 3);
    },

    charAnimation: function( Action , num ){
        var cache = cc.SpriteFrameCache.getInstance();
        cache.addSpriteFrames( s_naruto[0] , s_naruto[1] );

        var animFrames = [];
        for (var i = 0; i < num; i++) {
            var str = Action + i + ".png";
            var frame = cache.getSpriteFrame(str);
            animFrames.push(frame);
        }

        var animation = cc.Animation.create(animFrames, 0.1);
        return cc.Animate.create(animation);
    },
    
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },

    update: function() {
        //--------- Test ----------
        // var box = this.getChildByTag("BOX");
        // var thisRect = this.getBoundingBox();
        // box.setTextureRect( cc.rect( thisRect.x, thisRect.y, thisRect.width, thisRect.height));

        //--------- Test ----------
        if( ( this.health > 0 || !this.ground ) && this.delayStatus == 0){

            var oldRect = this.getBoundingBox();
            var oldX = this.x;
            var oldY = this.y;
            this.updateYMovement();
            this.updateXMovement();
        
            var dX = this.x - oldX;
            var dY = this.y - oldY;
        
            var newRect = cc.rect( oldRect.x + dX, oldRect.y + dY - 1, oldRect.width, oldRect.height + 1 );

            this.handleCollision( oldRect, newRect );
        
            this.updatePosition();
        }
        //===================== Check Animation ===========================
        this.updateAnimation(); 
        //===================== Check Animation ===========================
        this.updateDelay();
        // this.updateHealthBar();
        if( this.mana + 0.2 <= 1000 ){
            this.mana += 0.2;
        }else if( this.mana + 0.2 > 1000 ){
            this.mana = 1000;
        }
        if( this.health == 0){
            this.health = 0;
        }else
        if( this.health + 0.4 <= 1000 ){
            this.health += 0.4;
        }else if( this.health + 0.4 > 1000 ){
            this.health = 1000;
        }
    },

    randInt: function( Max, Min){
        return Math.floor(Min + (Math.random() * (Max - Min)));
    },

    attackMe: function( damage ){
        this._damageNew = damage - this.randInt( damage / 2, 0);
        this.health -= this._damageNew;
        
        if( this.health < 0 )
            this.health = 0;
    },
    checkOrAttack: function( a, b, c, d){
        return ( this._Attack1 && a ) || ( this._Attack2 && b ) || ( this._Attack3 && c ) || ( this._Attack4 && d );
    },

    checkAttack: function( damage ){
        this.enemySet.forEach( function( e, i ) {
            if( e._data[3] <= 0 ){
                this.enemySet.splice(i, 1);
                return;
            }
            var eRect = e.getBoundingBox();
            var thisRect = this.getBoundingBox();
            if( cc.rectIntersectsRect( eRect, thisRect ) ){
                e.damageToEnemy( damage, 20);
            }
        }, this );
    },
    updateDelay: function(){
        for( var i=0;i<this.delayWeapon.length;i++){
            if(this.delayWeapon[i] != this.maxDelayWepon[i]){
                this.delayWeapon[i]++;
            }
            
        }        
    },
    updateAnimation: function(){
        if( this.delayStatus == 0 ){
            if( !this.ground ){ // In the air
                if( this._Attack3 && !this._release ){
                    if( this.delayWeapon[this.weapon-1] == this.maxDelayWepon[this.weapon-1] ){
                            this.STATUS = Naruto.STATUS.THROWAIR;
                            this.delayStatus = 20;
                            this._release = true;
                            this.delayWeapon[this.weapon-1] = 0;
                        }
                }else if( this.vy <= 0 ){
                    this.STATUS = Naruto.STATUS.JUMPDOWN;
                }else{
                    this.STATUS = Naruto.STATUS.JUMPUP;
                }
            }else{ // on the ground
                if( this.health <= 0 ){
                    this.delayStatus = -1;
                    this.STATUS = Naruto.STATUS.DEAD;
                }else if( this.checkOrAttack( true, true, true, true ) && !this._release){
                    if( this._Attack1 ){
                        // Skill 1------------------
                        if( this.mana - 900 > 0){
                            this.STATUS = Naruto.STATUS.SKILL2;
                            this.delayStatus = 250;
                            this._release = true;
                            this.mana -= 900;
                            this.immortal = true;
                        }
                    }else if( this._Attack2 ){
                       // Skill 1------------------
                        if( this.mana - 300 > 0){
                            this.STATUS = Naruto.STATUS.SKILL1;
                            this.delayStatus = 140;
                            this._release = true;
                            this.mana -= 300;
                            this.immortal = false;
                        }
                    }else if( this._Attack3 ){
                        if( this.delayWeapon[this.weapon-1] == this.maxDelayWepon[this.weapon-1] ){
                            this.STATUS = Naruto.STATUS.THROW;
                            this.delayStatus = 25;
                            this._release = true;
                            this.delayWeapon[this.weapon-1] = 0;
                            this.immortal = false;
                        }
                    }else if( this._Attack4 ){
                        this.checkAttack( 200 );
                        this.STATUS = Naruto.STATUS.ATTACK;
                        this.delayStatus = 20;
                        this._release = true;      
                        this.immortal = false;                  
                    }
                }else if( this.vx < 0 ){
                    this.STATUS = Naruto.STATUS.RUNLEFT;
                    this.setAnchorPoint( cc.p( 0.5, 0 ) );
                    this.immortal = false;
                }else if( this.vx > 0 ){
                    this.STATUS = Naruto.STATUS.RUNRIGHT;
                    this.setAnchorPoint( cc.p( 0.5, 0 ) );
                    this.immortal = false;
                }else{
                    this.STATUS = Naruto.STATUS.STAND;
                    this.setAnchorPoint( cc.p( 0.5, 0 ) );
                    this.immortal = false;
                }
            }
            if(this.lastX < this.x){
                this.setFlippedX(false);
            }else if(this.lastX > this.x){
                this.setFlippedX(true);
            }
        }else if(this.delayStatus > 0 ){
            this.delayStatus--;
        }

        this.lastX = this.x;
        if( this.lastSTATUS != this.STATUS ){
            this.lastSTATUS = this.STATUS;
            this.stopAllActions();
            this.runAction(this.Action[this.STATUS]);
        }else{
            if(this.Action[this.lastSTATUS].isDone() && this.STATUS != Naruto.STATUS.DEAD ){         
                if( this.STATUS == Naruto.STATUS.SKILL1 ){
                    this.STATUS = Naruto.STATUS.USESKILL1;
                    this.delayStatus = 15;
                }else if( this.STATUS == Naruto.STATUS.SKILL2 ){
                    this.STATUS = Naruto.STATUS.USESKILL2;
                    this.delayStatus = 15;
                    this.setOpacity(0);
                    this.immortal = false;
                }
                this.runAction(this.Action[this.STATUS]);  
            }
        }
    },

    handleKeyDown: function( e ) {
        if ( Naruto.KEYMAP[ e ] != undefined ) {
            this[ Naruto.KEYMAP[ e ] ] = true;
        }
        
    },

    handleKeyUp: function( e ) {
        if ( Naruto.KEYMAP[ e ] != undefined ) {
            this[ Naruto.KEYMAP[ e ] ] = false;
        }
        if( e == cc.KEY.z || e == cc.KEY.c || e == cc.KEY.x || e == cc.KEY.v ){
            this._release = false;
        }
        
    },

    updateXMovement: function() {
        if ( this.ground ) {
            if ( this.delayStatus == 0 && !this.blockAttack){
                if ( ( !this.moveLeft ) && ( !this.moveRight ) ) {
                    this.autoDeaccelerateX();
                } else if ( this.moveRight ) {
                    this.accelerateX( 1 );
                } else if ( this.moveLeft ){
                    this.accelerateX( -1 );
                }
            }else{
                this.autoDeaccelerateX();
            }
        }else{
            if ( this.moveRight ) {
                this.accelerateX( 1 );
            } else if ( this.moveLeft ){
                this.accelerateX( -1 );
            }
        }
        this.x += this.vx;
        if ( this.x < 0 ) {
            this.x -= this.vx;
        }
        if ( this.x > 1559 ) {
            this.x -= this.vx;
        }
    },

    updateYMovement: function() {
        if ( this.ground ) {
            this.vy = 0;
            if ( this.jump ) {
                this.vy = this.jumpV;
                this.y = this.ground.getTopY() + this.vy;
                this.ground = null;
                this.jump2 = false;
                this.jump = false;
            }
        } else {
            if( !this.jump2 ){
                if ( this.jump ) {
                    this.vy = this.jumpV;
                    this.y = this.y + this.vy;
                    this.jump2 = true;
                }else{
                    this.vy += this.g;
                    this.y += this.vy;
                }
            }else{
                this.vy += this.g;
                this.y += this.vy;
            }
        }
    },

    isSameDirection: function( dir ) {
        return ( ( ( this.vx >=0 ) && ( dir >= 0 ) ) ||
                 ( ( this.vx <= 0 ) && ( dir <= 0 ) ) );
    },

    accelerateX: function( dir ) {
        if ( this.isSameDirection( dir ) ) {
            this.vx += dir * this.accX;
            if ( Math.abs( this.vx ) > this.maxVx ) {
                this.vx = dir * this.maxVx;
            }
        } else {
            if ( Math.abs( this.vx ) >= this.backAccX ) {
                this.vx += dir * this.backAccX;
            } else {
                this.vx = 0;
            }
        }
    },
    
    autoDeaccelerateX: function() {
        if ( Math.abs( this.vx ) < this.accX ) {
            this.vx = 0;
        } else if ( this.vx > 0 ) {
            this.vx -= this.accX;
        } else {
            this.vx += this.accX;
        }
    },

    handleCollision: function( oldRect, newRect ) {
        if ( this.ground ) {
            if ( !this.ground.onTop( newRect ) ) {
                this.ground = null;
            }
        } else {
            if ( this.vy <= 0 ) {
                var topBlock = this.findTopBlock( this.blocks, oldRect, newRect );
                
                if ( topBlock ) {
                    this.ground = topBlock;
                    this.y = topBlock.getTopY();
                    this.vy = 0;
                }
            }
        }
    },

    findTopBlock: function( blocks, oldRect, newRect ) {
        var topBlock = null;
        var topBlockY = -1;
        blocks.forEach( function( b ) {
            if ( b.hitTop( oldRect, newRect ) ) {
                if ( b.getTopY() > topBlockY ) {
                    topBlockY = b.getTopY();
                    topBlock = b;
                }
            }
        }, this );
        
        return topBlock;
    },

    setBlock: function ( blocks ){
        this.blocks = blocks;
    },
    setEnemy: function( enemys ){
        this.enemySet = enemys;
    },
    createHealthBar: function(){
        var bgBar = new cc.Sprite.create(s_healthbar);
        bgBar.setPosition( cc.p(0, 80));
        bgBar.setAnchorPoint( cc.p( 0, 0.5 ) );
        this.addChild(bgBar);

        var bar = new cc.Sprite();
        bar.setAnchorPoint( cc.p( 0, 0.5 ) );
        bar.setPosition( cc.p(1, 80));
        this.addChild(bar, 2, "Blood");
    },

    updateHealthBar: function(){   
        var bar = this.getChildByTag("Blood");
        if(this.health > 500){  
            bar.setColor( new cc.Color4B( 0, 222, 0, 255) );
        }else if(this.health > 200){
            bar.setColor( new cc.Color4B( 239, 198, 0, 255) );
        }else{
            bar.setColor( new cc.Color4B( 231, 16, 16, 255) );
        }
        bar.setTextureRect( cc.rect(1, 80, (this.health / 1000)*59, 3) );
    },

    isAttack: function(){
        return this._Attack1 || this._Attack2 || this._Attack3 || this._Attack4;
    }
});
Naruto.STATUS ={
    DEAD: 0,
    STAND: 1,
    RUNLEFT: 2,
    RUNRIGHT: 3,
    JUMPUP: 4,
    JUMPDOWN: 5,
    ATTACK:6,
    THROW: 7,
    THROWAIR: 8,
    SKILL1: 9,
    SKILL2: 10,
    USESKILL1: 11,
    USESKILL2: 12,
};
Naruto.KEYMAP = {};
Naruto.KEYMAP[cc.KEY.left] = 'moveLeft';
Naruto.KEYMAP[cc.KEY.right] = 'moveRight';
Naruto.KEYMAP[cc.KEY.space] = 'jump';
Naruto.KEYMAP[cc.KEY.z] = '_Attack1';
Naruto.KEYMAP[cc.KEY.x] = '_Attack2';
Naruto.KEYMAP[cc.KEY.c] = '_Attack3';
Naruto.KEYMAP[cc.KEY.v] = '_Attack4';

SkillNaruto = cc.Sprite.extend({
    _type: null,
    _direction: null,
    _cache: null,
    x: null,
    y: null,
    _status: "start",
    _rotation: 0,
    _hit: null,
    enemySet: [],
    ctor: function( number, direction, x, y ) {
        this._super();
        this._type = number;
        this._direction = direction;
        this.init();

        this.x = x;
        this.y = y;
        this.origin_x = x;

        this.scheduleUpdate();
    },
    
    init: function(){
        this._cache = cc.SpriteFrameCache.getInstance();
        this._cache.addSpriteFrames( s_naruto[0] , s_naruto[1] );

        this._damage = [ 150, 80];
        this._frame = [ 2, 20];
        this._length = [ 1000, 500];
        this._speed = [ 12, 10];
        this._end = [ ];

        this.setFlippedX( this._direction );
        this.setAnchorPoint( cc.p( 0.5, 0.5 ) );
        this._action = this.AnimationWeapon( this._type, this._frame[ this._type -1 ]);
        this.runAction( this._action );
        if( ( this._type == 2 ) ){
            this.setRotation( 0 );
        }
        this._status = "normal";
        // ------- Test-------------
        // var box = new cc.Sprite();
        // var thisRect = this.getBoundingBox();
        // box.setColor(cc.BLUE);
        // box.setTextureRect( cc.rect( thisRect.x, thisRect.y, thisRect.width, thisRect.height));
        // box.setOpacity(100);
        // this.addChild(box,10,"BOX");
        // ------- Test-------------
    },

    setEnemy: function( enemy ){
        this.enemySet = enemy;
    },

    checkHitEnemy: function(){
        var boolean = false;
        this.enemySet.forEach( function( e, i ) {
            if( e._data[3] <= 0 ){
                this.enemySet.splice(i, 1);
                return;
            }
            var eRect = e.getBoundingBox();
            var thisRect = this.getBoundingBox();
            if( cc.rectIntersectsRect( eRect, thisRect ) ){
                boolean = true;
                e.damageToEnemy( this._damage[0], 100);
            }
        }, this );
        return boolean;
    },
    checkHitEnemy2: function(){
        var boolean = false;
        this.enemySet.forEach( function( e, i ) {
            if( e._data[3] <= 0 ){
                this.enemySet.splice(i, 1);
                return;
            }
            var eRect = e.getBoundingBox();
            var thisRect = this.getBoundingBox();
            if( cc.rectIntersectsRect( eRect, thisRect ) ){
                boolean = true;
                e.damageToEnemy( this._damage[1], 10);
            }
        }, this );
        return boolean;
    },

    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },

    getDamge: function( i ){
        return this._damge[i];
    },

    AnimationWeapon: function( number, num ){
        var animFrames = [];
        for (var i = 0; i < num; i++) {
            var str = "w_Skill" + number +"_"+ i + ".png";
            var frame = this._cache.getSpriteFrame(str);
            animFrames.push(frame);
        }
        var animation = cc.Animation.create(animFrames, 0.1);
        return cc.Animate.create(animation);
    },

    endEffect: function(){
        switch( this._type ){
            case 1:
            break;
            case 2:
            break;
        }
    },

    update: function(){

         //--------- Test ----------
        // var box = this.getChildByTag("BOX");
        // var thisRect = this.getBoundingBox();
        // box.setTextureRect( cc.rect( thisRect.x, thisRect.y, thisRect.width, thisRect.height));

        //--------- Test ----------

        this.updatePosition();
        if( this._direction && this._status == "normal" ){
            if (!( this.x < 20 ) && !(this.x > 1539)) {
                this.x -= this._speed[ this._type - 1 ];
            }
            
        }else if( !this._direction && this._status == "normal" ){
            if (!( this.x < 20 ) && !(this.x > 1539) ){
                this.x += this._speed[ this._type - 1 ];
            }
        }
        if( this._type == 1 ){
            if( ( Math.abs( this.origin_x - this.x ) >= this._length[ this._type - 1 ] || this.checkHitEnemy() )  && this._status == "normal" ){
                this.endEffect();
                this._status = "end";
            }
        }else if( this._type == 2){
            this.checkHitEnemy2();
            if( this._action.isDone() && this._status == "normal" ){
                this.endEffect();
                this._status = "end";
            }
        }
        if( this._status == "end" ){
            switch( this._type ){
            case 1:         
            this.removeFromParent();
            break;
            case 2:
            this.removeFromParent();
            break;
            // case 3:
            // this.removeFromParent();
            // break;
            // case 4:
            // if( this._action.isDone() ){
            //     this.removeFromParent();
            // }
            // break;
            }
        }else if( this._action.isDone() ){
            switch( this._type ){
            case 1:         
            this.runAction( this._action );
            break;
            case 2:
            this.removeFromParent();
            break;
            }
        }
    },
});