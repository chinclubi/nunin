var StartScene = cc.Scene.extend({
    ctor:function () {
        this._super();

        this.addChild(new BackgroundLayer(), 0, TagOfLayer.BackgroundLayer);
        this.addChild(new GameLayer(), 0, TagOfLayer.GameLayer);        
        this.addChild(new EffectLayer(), 0, TagOfLayer.EffectLayer);

        this.follow();
        this.scheduleUpdate();
        this.init();
        this.waitShow = true;
    },

    init: function(){
        var backgroundLayer = this.getChildByTag(TagOfLayer.BackgroundLayer);
        var gameLayer = this.getChildByTag(TagOfLayer.GameLayer);
        backgroundLayer.runAction(cc.Follow.create(gameLayer.character, cc.rect(0, 0, 1555, 600)));
        gameLayer.runAction(cc.Follow.create(gameLayer.character, cc.rect(0, 0, 1555, 600)));
    },

    endGame:function(){
    	var gameLayer = this.getChildByTag(TagOfLayer.GameLayer);
    	var backgroundLayer = this.getChildByTag(TagOfLayer.BackgroundLayer);
        if(gameLayer.character.health <= 0 || gameLayer.boss.health <= 0){
            if(this.waitShow){
                this.waitLayer = new waitScreen();
                this.waitShow = false;  
                this.waitLayer.init();
                this.addChild( this.waitLayer , 1000 );
                gameLayer.setKeyboardEnabled( false );
                backgroundLayer.runAction(cc.Follow.create(this.waitLayer, cc.rect(0, 0, 1555, 600)));
            }
        }
        // if(this.socket.socket.connected){
        //     if(this.waitShow){
        //         gameLayer.IOcreateRegisNew(); 
        //         this.waitShow = false;
        //         this.removeChild( this.waitLayer );
        //         gameLayer.setKeyboardEnabled( true );
        //         backgroundLayer.runAction(cc.Follow.create(gameLayer.character, cc.rect(0, 0, 1555, 600)));
        //         this.reconnectTime[0] = 0;
        //     }
        // }else{
        //     if( this.reconnectTime[0] <= 1440 ){
        //         this.socket.socket.connect();
        //         if(!this.waitShow){
        //             this.waitLayer = new waitScreen();
        //             this.waitShow = true;  
        //             this.waitLayer.init();
        //             this.addChild( this.waitLayer , 1000 );
        //             gameLayer.setKeyboardEnabled( false );
        //             backgroundLayer.runAction(cc.Follow.create(this.waitLayer, cc.rect(0, 0, 1555, 600)));
        //         }
        //         this.reconnectTime[0]++;
        //     }else{
        //         this.waitLayer.setTime( true );
        //     }
        // }
    },

    follow: function(){
        var backgroundLayer = this.getChildByTag(TagOfLayer.BackgroundLayer);
    	var gameLayer = this.getChildByTag(TagOfLayer.GameLayer);
    	backgroundLayer.runAction(cc.Follow.create(gameLayer.character, cc.rect(0, 0, 1555, 600)));
    },

    update: function(){
    	this.endGame();
    }

});