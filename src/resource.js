var s_naruto = ["plist/naruto.plist","plist/naruto.png"];
var s_loading = ["plist/loading.plist","plist/loading.png"];
var s_cartoon = ["plist/cartoon.plist","plist/cartoon.png"];
var loader_powerby = ["img/powered.png","img/powerby.png"];
var s_weapon = ["plist/weapon.plist","plist/weapon.png"];
var s_enemy_c1 = ["plist/enemyC1.plist","plist/enemyC1.png"];
var s_boss = ["plist/boss.plist","plist/boss.png"];
var s_logo = "img/logo_nunin.png";
var s_healthbar = "img/healthBar.png";
var s_bar = ["img/health_Bar_1.png", "img/health_Bar_2.png", "img/Bar_circle.png", "img/mana_Bar_1.png", "img/mana_Bar_2.png"];
var s_icon_weapon = ["img/iconWeapon1.png", "img/iconWeapon2.png", "img/iconWeapon3.png", "img/iconWeapon4.png"];
var s_icon_weapon_r = ["img/iconWeapon1_red.png", "img/iconWeapon2_red.png", "img/iconWeapon3_red.png", "img/iconWeapon4_red.png"];
var s_menu_bg = "img/bg_menu.png";
var s_barHP = "img/Bar.png";
var s_bg_loading = "img/bg_loading.png";

var g_resources = [
    //image

    {src:s_menu_bg},
    {src:s_barHP},
    {src:s_icon_weapon[0]},
    {src:s_icon_weapon[1]},
    {src:s_icon_weapon[2]},
    {src:s_icon_weapon[3]},
    {src:s_icon_weapon_r[0]},
    {src:s_icon_weapon_r[1]},
    {src:s_icon_weapon_r[2]},
    {src:s_icon_weapon_r[3]},
    {src:s_bar[0]},
    {src:s_bar[1]},
    {src:s_boss[0]},
    {src:s_boss[1]},
    {src:s_weapon[0]},
    {src:s_weapon[1]},
    {src:s_enemy_c1[0]},
    {src:s_enemy_c1[1]},
    {src:s_bg_loading},
    {src:s_logo},
    {src:s_healthbar},
    {src:loader_powerby[0]},
    {src:loader_powerby[1]},
    {src:s_naruto[0]},
    {src:s_naruto[1]},
    {src:s_loading[0]},
    {src:s_loading[1]},
    {src:s_cartoon[0]},
    {src:s_cartoon[1]},
];