EnemyClass1 = cc.Sprite.extend({
    moveLeft: false,
    moveRight: false,
    jump: false,
    attack: false,
    lastSTATUS: null,
    _character: null,
    _sprite: null,
    _timeBar: 0,
    _damageNew: 0,
    _data: null, // 0 - Start delay // 1 - Speed (maxVx) // 2 - Delay Sprite Frame // 3 - Health // 4 - Delay when was attack // 5 - Damage
    ctor: function( blocks ) {
        this._super();
        this.selectEnemy();
        this.setAnchorPoint( cc.p( 0.5, 0 ) );
        this.blocks = [];
        this.blocks = blocks;
        this.initEnemy();
        this.lastX = this.x;

        this.accX = 0.25;
        this.backAccX = 0.5;
        this.jumpV = 15;
        this.g = -1;
        
        this.vx = 0;
        this.vy = 0;

        this.ground = null;
        
        this.STATUS = EnemyClass1.STATUS.START;

        this.character = null;

        this.charID = null;

        
        this.init();

        this.delayStatus = this._data[0];
        this.delayAI = this._data[0];
        
    },
    
    init: function(){
        this.updatePosition();

        this.Action = new Array();
        this.Action[0] = this.charAnimation( this._character + "_Start" , this._sprite[0] );
        this.Action[1] = this.charAnimation( this._character + "_Dead" , this._sprite[1] );
        this.Action[2] = this.charAnimation( this._character + "_Stand" , this._sprite[2] );
        this.Action[3] = this.charAnimation( this._character + "_Run" , this._sprite[3] );
        this.Action[4] = this.charAnimation( this._character + "_Run" , this._sprite[4] );
        this.Action[5] = this.charAnimation( this._character + "_Attack" , this._sprite[5] );

        this.createHealthBar();

        // ------- Test-------------
        // var box = new cc.Sprite();
        // var thisRect = this.getBoundingBox();
        // box.setColor(cc.RED);
        // box.setTextureRect( cc.rect( thisRect.x, thisRect.y, thisRect.width, thisRect.height));
        // box.setOpacity(100);
        // this.addChild(box,10,"BOX");
        // ------- Test-------------
    },

    createHealthBar: function(){
        var bgBar = new cc.Sprite.create(s_healthbar);
        bgBar.setPosition( cc.p(0, 80));
        bgBar.setAnchorPoint( cc.p( 0, 0.5 ) );
        this.addChild(bgBar, 2,"bgBlood");

        var bar = new cc.Sprite();
        bar.setAnchorPoint( cc.p( 0, 0.5 ) );
        bar.setPosition( cc.p(1, 80));
        this.addChild(bar, 2, "Blood");
    },

    updateHealthBar: function(){
        var bar = this.getChildByTag("Blood");
        var bg = this.getChildByTag("bgBlood");

        if( this._timeBar > 0 ){   
            bg.setOpacity(255);
            bar.setOpacity(255);

            if( this._character == "E1") var fullBlood = 2500;
            else if( this._character == "E2") var fullBlood = 3000;

            if(this._data[3] > fullBlood/2 ){  
                bar.setColor( new cc.Color4B( 0, 222, 0, 255) );
            }else if(this._data[3] > fullBlood/4 ){
                bar.setColor( new cc.Color4B( 239, 198, 0, 255) );
            }else if(this._data[3] > 0 ){
                bar.setColor( new cc.Color4B( 231, 16, 16, 255) );
            }else{
                bg.setOpacity(0);
            }
            bar.setTextureRect( cc.rect(1, 80, (this._data[3] / fullBlood)*59, 3) );
            this._timeBar--;
        }else{
            bg.setOpacity(0);
            bar.setOpacity(0);
        }
    },

    selectEnemy: function(){
        if( this.randInt( 10000, 0) > 3000 ){
            this._character = "E1";
            this._sprite = [ 10, 12, 5, 6, 6, 5];
            this._data = [ 60, 1.2, 0.1, 2500, 50, 100]; 
        }else{
            this._character = "E2";
            this._sprite = [ 8, 7, 3, 5, 5, 5];
            this._data = [ 60, 0.5, 0.15, 3000, 20, 200]; 
        }
    },

    initEnemy: function(){
        this.y = this.blocks[this.randInt( this.blocks.length - 1, 0)].getTopY() + 10;
        var rect = this.blocks[this.randInt( this.blocks.length - 1 , 0)].getBoundingBox();
        this.x = this.randInt( cc.rectGetMaxX( rect ), cc.rectGetMinX( rect ));
    },

    randInt: function( Max, Min){
        return Math.floor(Min + (Math.random() * (Max - Min)));
    },

    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },

    setCharacter: function( character ){
        this.character = character;
    },

    checkAttack: function(){
        var charRect = this.character.getBoundingBox();
        var enemyRect = this.getBoundingBox();
        if( this.delayStatus == 0 && cc.rectIntersectsRect( charRect, enemyRect ) ){
            this.character.attackMe( this._data[5] );
        }
    },
    update: function() {
        //--------- Test ----------
        // var box = this.getChildByTag("BOX");
        // var thisRect = this.getBoundingBox();
        // box.setTextureRect( cc.rect( thisRect.x, thisRect.y, thisRect.width, thisRect.height));

        //--------- Test ----------

        if( this.delayAI == 0 ){
            if( this.playerBetweenX( 500 ) && this.playerBetweenY( 10 ) && this.character.health > 0 && !this.character.immortal){
                if( this.playerBetweenX( 30 ) && this.y == this.character.y ){
                    this.setAction( false, false, false);
                    this.attack = true;
                    this.delayAI = 50;
                }else if( this.x > this.character.x  ){
                    this.setAction( true, false, false);
                   // this.delayAI = 20;
                }else if( this.x <= this.character.x ){
                    this.setAction( false, true, false);
                   // this.delayAI = 20;
                }
            }else{
                var rand = this.randInt( 1000, 0);
                if( rand > 997 ){
                    this.setAction( true, false, false);
                    this.delayAI = this.randInt( 50, 30);
                }else if( rand > 994 ){
                    this.setAction( false, true, false);
                    this.delayAI = this.randInt( 50, 30);
                }else{
                    this.moveLeft = false;
                    this.moveRight = false;
                    this.jump = false;
                }
            }
        }else if( this.delayAI > 0 ){
            this.delayAI--;            
        }
        //================================== AI =======================================

        if( this._data[3] > 0 ){

            var oldRect = this.getBoundingBox();
            var oldX = this.x;
            var oldY = this.y;
            this.updateYMovement();
            this.updateXMovement();
            
            var dX = this.x - oldX;
            var dY = this.y - oldY;
        
            var newRect = cc.rect( oldRect.x + dX,
                               oldRect.y + dY - 1,
                               oldRect.width,
                               oldRect.height + 1 );

            this.handleCollision( oldRect, newRect );
            this.updatePosition();
        }
        this.updateHealthBar();
        //===================== Check Animation ===========================
        if( this.delayStatus == 0 ){
            if( this._data[3] <= 0 ){ // Enemy Dead
                this.STATUS = EnemyClass1.STATUS.DEAD;
                this.delayStatus = -1;
                this.delayAI = -1;
                this.health = 0;
                this.setAction( false, false, false);
            }else if( this.attack ){
                this.checkAttack();
                this.delayStatus = 50;
                this.STATUS = EnemyClass1.STATUS.ATTACK;
            }else if( this.vx > 0 ){
                this.setFlippedX(true);
                this.STATUS = EnemyClass1.STATUS.RUNLEFT;
            }else if( this.vx < 0 ){
                this.setFlippedX(false);
                this.STATUS = EnemyClass1.STATUS.RUNRIGHT;
            }else{
                this.STATUS = EnemyClass1.STATUS.STAND;
            }
        }else if( this.delayStatus > 0 ){
            this.delayStatus -= 1;
        }
        this.lastX = this.x;
        if( this.lastSTATUS != this.STATUS ){
            this.lastSTATUS = this.STATUS;
            this.stopAllActions();
            this.runAction(this.Action[this.STATUS]);
        }else{
            if(this.Action[this.STATUS].isDone() && this.STATUS != EnemyClass1.STATUS.DEAD ){
                this.runAction(this.Action[this.STATUS]);  
                if( this.attack ){
                    this.attack = false;
                }
            }
        }

        if( this._character == "E1" && this.STATUS == EnemyClass1.STATUS.DEAD && this.Action[this.lastSTATUS].isDone() ){
            this.removeFromParent();
        }
        //===================== Check Animation ===========================
    },
    enemyBetweenY: function( range ){
        return this.y >= this.character.y - range && this.y <= this.character.y + range ;
    },

    playerBetweenY: function( range ){
        return this.character.y >= this.y - range && this.character.y <= this.y + range ;
    },

    enemyBetweenX: function( range ){ // ||||| Player ||||| Are enemy in the range or not?
        return this.x >= this.character.x - range && this.x <= this.character.x + range ;
    },

    playerBetweenX: function( range ){ // ||||| Enemy ||||| Are player in the range or not?
        return this.character.x >= this.x - range && this.character.x <= this.x + range ;
    },

    setAction: function( left, right, jump ){
        this.moveRight = right;
        this.moveLeft = left;
        this.jump = jump;
    },

    damageToEnemy: function( damage, stunt){
        this._timeBar = 150;

        this._damageNew = damage - this.randInt( damage / 3, 0);
        this._data[3]  -= this._damageNew;

        if( this._data[3] > 0 ){
            this.delayAI = this._data[4];
        }
        this.setAction( false, false, false);
        this.delayAI = 50;
        if( this.character.isFlippedX() ){
            this.x -= 20;
        }else{
            this.x += 20;            
        }
    },

    handleKeyDown: function( e ) {
        if ( Enemy.KEYMAP[ e ] != undefined ) {
            this[ Enemy.KEYMAP[ e ] ] = true;
        }
    },

    handleKeyUp: function( e ) {
        if ( Enemy.KEYMAP[ e ] != undefined ) {
            this[ Enemy.KEYMAP[ e ] ] = false;
        }
    },

    updateXMovement: function() {
        if ( this.ground ) {
            if ( ( !this.moveLeft ) && ( !this.moveRight ) ) {
                this.autoDeaccelerateX();
            } else if ( this.moveRight ) {
                this.accelerateX( 1 );
            } else if ( this.moveLeft ){
                this.accelerateX( -1 );
            }
        }else{
            if ( this.moveRight ) {
                this.accelerateX( 1 );
            } else if ( this.moveLeft ){
                this.accelerateX( -1 );
            }
        }
        this.x += this.vx;
        if ( this.x < 0 ) {
            this.x -= this.vx;
        }
        if ( this.x > 1559 ) {
            this.x -= this.vx;
        }
    },

    updateYMovement: function() {
        if ( this.ground ) {
            this.vy = 0;
            if ( this.jump ) {
                this.vy = this.jumpV;
                this.y = this.ground.getTopY() + this.vy;
                this.ground = null;
                this.jump2 = false;
                this.jump = false;
            }
        } else {
            if( !this.jump2 ){
                if ( this.jump ) {
                    this.vy = this.jumpV;
                    this.y = this.y + this.vy;
                    this.jump2 = true;
                }else{
                    this.vy += this.g;
                    this.y += this.vy;
                }
            }else{
                this.vy += this.g;
                this.y += this.vy;
            }
        }
    },
    isSameDirection: function( dir ) {
        return ( ( ( this.vx >=0 ) && ( dir >= 0 ) ) ||
                 ( ( this.vx <= 0 ) && ( dir <= 0 ) ) );
    },

    accelerateX: function( dir ) {
        if ( this.isSameDirection( dir ) ) {
            this.vx += dir * this.accX;
            if ( Math.abs( this.vx ) > this._data[1] ) {
                this.vx = dir * this._data[1];
            }
        } else {
            if ( Math.abs( this.vx ) >= this.backAccX ) {
                this.vx += dir * this.backAccX;
            } else {
                this.vx = 0;
            }
        }
    },
    
    autoDeaccelerateX: function() {
        if ( Math.abs( this.vx ) < this.accX ) {
            this.vx = 0;
        } else if ( this.vx > 0 ) {
            this.vx -= this.accX;
        } else {
            this.vx += this.accX;
        }
    },

    handleCollision: function( oldRect, newRect ) {
        if ( this.ground ) {
            if ( !this.ground.onTop( newRect ) ) {
                this.ground = null;
            }
        } else {
            if ( this.vy <= 0 ) {
                var topBlock = this.findTopBlock( this.blocks, oldRect, newRect );
                
                if ( topBlock ) {
                    this.ground = topBlock;
                    this.y = topBlock.getTopY();
                    this.vy = 0;
                }
            }
        }
    },

    findTopBlock: function( blocks, oldRect, newRect ) {
        var topBlock = null;
        var topBlockY = -1;
        
        blocks.forEach( function( b ) {
            if ( b.hitTop( oldRect, newRect ) ) {
                if ( b.getTopY() > topBlockY ) {
                    topBlockY = b.getTopY();
                    topBlock = b;
                }
            }
        }, this );
        
        return topBlock;
    },

    setBlock: function ( blocks ){
        this.blocks = blocks;
    },

    charAnimation: function( Action , num ){
        var cache = cc.SpriteFrameCache.getInstance();
        cache.addSpriteFrames( s_enemy_c1[0] , s_enemy_c1[1] );
        
        var animFrames = [];
        for (var i = 0; i < num; i++) {
            var str = Action + i + ".png";
            var frame = cache.getSpriteFrame(str);
            animFrames.push(frame);
        }

        var animation = cc.Animation.create(animFrames, this._data[2] );
        return cc.Animate.create(animation);
    }
});
EnemyClass1.STATUS ={
    START: 0,
    DEAD: 1,
    STAND: 2,
    RUNLEFT: 3,
    RUNRIGHT: 4,
    ATTACK: 5,
};