Weapon = cc.Sprite.extend({
	_type: null,
	_direction: null,
	_cache: null,
	x: null,
	y: null,
	_status: "normal",
	_rotation: 0,
    _hit: null,
    enemySet: [],
	ctor: function( number, direction, x, y ) {
        this._super();
        this._type = number;
        this._direction = direction;
        this.init();

        this.x = x;
        this.y = y;
        this.origin_x = x;

        this.scheduleUpdate();
    },
    
    init: function(){
    	this._cache = cc.SpriteFrameCache.getInstance();
        this._cache.addSpriteFrames( s_weapon[0] , s_weapon[1] );

        this._damage = [ 40, 80, 20, 100 ];
        this._frame = [ 11, 9, 5, 2 ];
        this._length = [ 400, 500, 700, 350 ];
        this._speed = [ 12, 15, 12, 10];
        this._end = [ ];

        this.setFlippedX( this._direction );
        this.setAnchorPoint( cc.p( 0.5, 0.5 ) );
        this._action = this.AnimationWeapon( this._type, this._frame[ this._type -1 ]);
        this.runAction( this._action );

        // ------- Test-------------
        // var box = new cc.Sprite();
        // var thisRect = this.getBoundingBox();
        // box.setColor(cc.BLUE);
        // box.setTextureRect( cc.rect( thisRect.x, thisRect.y, thisRect.width, thisRect.height));
        // box.setOpacity(100);
        // this.addChild(box,10,"BOX");
        // ------- Test-------------
    },

    setEnemy: function( enemy ){
        this.enemySet = enemy;
    },

    checkHitEnemy: function(){
        var boolean = false;
        this.enemySet.forEach( function( e, i ) {
            if( e._data[3] <= 0 ){
                this.enemySet.splice(i, 1);
                return;
            }
            var eRect = e.getBoundingBox();
            var thisRect = this.getBoundingBox();
            if( cc.rectIntersectsRect( eRect, thisRect ) ){
                boolean = true;
                e.damageToEnemy( 100, 50);
            }
        }, this );
        return boolean;
    },

    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },

    getDamge: function( i ){
    	return this._damge[i];
    },

    AnimationWeapon: function( number, num ){
    	var animFrames = [];
        for (var i = 0; i < num; i++) {
            var str = "Weapon" + number +"_"+ i + ".png";
            var frame = this._cache.getSpriteFrame(str);
            animFrames.push(frame);
        }
        var animation = cc.Animation.create(animFrames, 0.1);
        return cc.Animate.create(animation);
    },
    animationEndWeapon4 : function(){
    	var Frame = [];
        	for (var i = 2; i < 9; i++) {
           		var str = "Weapon4_"+ i + ".png";
            	var frame = this._cache.getSpriteFrame(str);
            	Frame.push(frame);
        	}
        var animation = cc.Animation.create(Frame, 0.1);
        return cc.Animate.create(animation);
    },
    endEffect: function(){
    	switch( this._type ){
    		case 1:
    		break;
    		case 2:
    		break;
    		case 3:
    		break;
    		case 4:
    		this.stopAllActions();
    		this._action = this.animationEndWeapon4();
    		this.runAction( this._action );
    		break;
    	}
    },

    update: function(){

         //--------- Test ----------
        // var box = this.getChildByTag("BOX");
        // var thisRect = this.getBoundingBox();
        // box.setTextureRect( cc.rect( thisRect.x, thisRect.y, thisRect.width, thisRect.height));

        //--------- Test ----------

    	this.updatePosition();
    	
    	if( this._direction && this._status == "normal" ){
    		this.x -= this._speed[ this._type - 1 ];
    	}else if( !this._direction && this._status == "normal" ){
    		this.x += this._speed[ this._type - 1 ];
    	}

    	if( ( this._type == 2 || this._type == 3 ) && this._status == "normal" ){
    		this._rotation += 15;
    		this.setRotation( this._rotation );
    	}
    	if( ( Math.abs( this.origin_x - this.x ) >= this._length[ this._type - 1 ]  || this.checkHitEnemy() )  && this._status == "normal" ){
    		this.endEffect();
    		this._status = "end";
    	}
    	if( this._status == "end" ){
    		switch( this._type ){
    		case 1:    		
    		this.removeFromParent();
    		break;
    		case 2:
    		this.removeFromParent();
    		break;
    		case 3:
    		this.removeFromParent();
    		break;
    		case 4:
    		if( this._action.isDone() ){
    			this.removeFromParent();
    		}
    		break;
    		}
    	}else if( this._action.isDone() ){
    		this.runAction( this._action );
    	}
    },
});