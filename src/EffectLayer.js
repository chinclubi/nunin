var EffectLayer = cc.Layer.extend({
	_health: 0,
	_release: false,
	_skill: false,
	ctor: function(){
		this._super();
        this.init();
        //this.setKeyboardEnabled( true );
	},

	init: function() {

        var bar = cc.Sprite.create( s_barHP );
        bar.setAnchorPoint( cc.p( 0, 1 ) );
        bar.setPosition(cc.p(10,590));
        this.addChild( bar );

        this.createHealthBar();
        this.createIcon();
        this.scheduleUpdate();

    	return true;
	},
	currentWeapon: function(){
		if( this.icon1._status == 1)
			return 1;
		else if( this.icon2._status == 1 )
			return 2;
		else if( this.icon3._status == 1 )
			return 3;
		else if( this.icon4._status == 1 )
			return 4;
	},
	createIcon: function(){
		this.icon1 = new iconItem(1,1);
		this.icon1.setPosition( cc.p( 220, 440));
		this.addChild(this.icon1, 4);

		this.icon2 = new iconItem(2,2);
		this.icon2.setPosition( cc.p( 173, 443));
		this.addChild(this.icon2, 4);

		this.icon3 = new iconItem(3,3);
		this.icon3.setPosition( cc.p( 220, 440));
		this.addChild(this.icon3, 4);

		this.icon4 = new iconItem(4,4);
		this.icon4.setPosition( cc.p( 270, 440));
		this.addChild(this.icon4, 4);
	},
	update: function(){
		var bar = this.getChildByTag("Blood");
		var bar2 = this.getChildByTag("Blood_2");
		var _gameLayer = this.getParent().getChildByTag(TagOfLayer.GameLayer);
		this._health = _gameLayer.character.health;
		
		_gameLayer.character.weapon = this.currentWeapon();

		bar.setTextureRect( cc.rect( 0, 0, Math.floor(( 227 / 1000 ) * this._health ) , 29) );
		if(this._health != 0){
			bar2.setPosition( cc.p( bar.getBoundingBox().x + bar.getBoundingBox().width , bar2.getPosition().y ));
		}else{
			bar2.setPosition( cc.p( bar.getBoundingBox().x - 10 , bar2.getPosition().y ));
		}

		this._mana = _gameLayer.character.mana;
		var mana = this.getChildByTag("Mana");
		var mana2 = this.getChildByTag("Mana_2");
		mana.setTextureRect( cc.rect( 0, 0, Math.floor(( 151 / 1000 ) * this._mana ) , 12) );
		if(this._mana != 0){
			mana2.setPosition( cc.p( mana.getBoundingBox().x + mana.getBoundingBox().width , mana.getPosition().y ));
		}else{
			this.removeChild( mana2 );
		}
		
        var delay = _gameLayer.character.delayWeapon;
        var maxdelay = _gameLayer.character.maxDelayWepon;
        this.icon1.setPercentage( delay, maxdelay);
        this.icon2.setPercentage( delay, maxdelay);
        this.icon3.setPercentage( delay, maxdelay);
        this.icon4.setPercentage( delay, maxdelay);
		
	},
	
	onKeyDown: function( e ) {
		if( !this._release ){
			switch(e){
				case cc.KEY.down:
				this.icon1.setStatus(this.icon1.getStatus()-1);
				this.icon2.setStatus(this.icon2.getStatus()-1);
				this.icon3.setStatus(this.icon3.getStatus()-1);
				this.icon4.setStatus(this.icon4.getStatus()-1);
				break;
				case cc.KEY.up:
				this.icon1.setStatus(this.icon1.getStatus()+1);
				this.icon2.setStatus(this.icon2.getStatus()+1);
				this.icon3.setStatus(this.icon3.getStatus()+1);
				this.icon4.setStatus(this.icon4.getStatus()+1);
				break;
			}
			this._release = true;
		}
	},
	onKeyUp: function( e ) {
		if( e == cc.KEY.down || e == cc.KEY.up ){
			this._release = false;
		}
	},
	createHealthBar: function(){
        var healthBar = new cc.Sprite();
        healthBar.initWithFile( s_bar[0] , cc.rect( 0, 0, 227, 29) );
        healthBar.setPosition( cc.p( 130, 520));
        healthBar.setAnchorPoint( cc.p( 0, 0.5 ) );
        this.addChild( healthBar, 1, "Blood");

         var healthBar_2 = new cc.Sprite();
        healthBar_2.initWithFile( s_bar[1] , cc.rect( 0, 0, 10, 29) );
        healthBar_2.setPosition( cc.p( 357, 520));
        healthBar_2.setAnchorPoint( cc.p( 0, 0.5 ) );
        this.addChild( healthBar_2, 1, "Blood_2");

        var bar_Circle = new cc.Sprite.create(s_bar[2]);
        bar_Circle.setAnchorPoint( cc.p( 0, 0.5 ) );
        bar_Circle.setPosition( cc.p(17, 512));
        
        this.addChild( bar_Circle, 2, "Circle");

        var manaBar = new cc.Sprite();
        manaBar.initWithFile( s_bar[3] , cc.rect( 0, 0, 151, 12) );
        manaBar.setAnchorPoint( cc.p( 0, 0.5 ) );
        manaBar.setPosition( cc.p(147, 480));
        this.addChild( manaBar, 2, "Mana");

        var manaBar_2 = new cc.Sprite();
        manaBar_2.initWithFile( s_bar[4] , cc.rect( 0, 0, 4, 12) );
        manaBar_2.setAnchorPoint( cc.p( 0, 0.5 ) );
        manaBar_2.setPosition( cc.p(298, 480));
        this.addChild( manaBar_2, 2, "Mana_2");
    },
});
iconItem = cc.Sprite.extend({
	_type: null,
	_lastStatus: null,
	ctor: function( type, status){
		this._super();
		this._type = type;
		this.delay();
		this.setAnchorPoint( cc.p( 0.5, 0.5 ) );
		
		this._cache = cc.SpriteFrameCache.getInstance();
        this._cache.addSpriteFrames( s_weapon[0] , s_weapon[1] );
        this.createWeapon();

        this._status = status;
        this._lastStatus = status;
        this.init();
        this.scheduleUpdate();
	},
	init: function(){
		if(this._status == 1){
			this.setScale(2);
		}
		if(this._status == 3){
			this.setOpacity(0);
			this.red.setOpacity(0);
		}
		
	},
	setPercentage: function( current, max){
		this.red.setPercentage((current[this._type-1]/max[this._type-1])*100);
	},
    setStatus: function( n ){
    	if( n <= 4 && n >= 1 )
    		this._status = n;
    	else{
    		if( n <= 1 ){
    			this._status = 4;	
    		}else{
    			this._status = 1;	
    		}
    		
    	}
    },
    getStatus: function(){
    	return this._status;
    },
	update: function(){
		var _x = this.getPosition().x;
		var _y = this.getPosition().y;			
		if(this._status == 1 && this._lastStatus != this._status){
			if( _x != 220 || _y != 440){
				var action = cc.MoveTo.create(0.2,cc.p( 220, 440));
				var action2 = cc.ScaleTo.create(0.2, 2, 2);				
				this.runAction( action );
				this.runAction( action2 );
				this._lastStatus = this._status;				
			}
		}else if(this._status == 2 && this._lastStatus != this._status ){
			if( _x != 175 || _y != 443){
				this.setOpacity(255);
				this.red.setOpacity(255);
				var action = cc.MoveTo.create(0.2,cc.p( 175, 443));
				var action2 = cc.ScaleTo.create(0.2, 1, 1);
				this.runAction( action );
				this.runAction( action2 );
				this._lastStatus = this._status;				
			}
		}else if(this._status == 3 && this._lastStatus != this._status){
			if( _x != 220 || _y != 439){
				this.setOpacity(0);
				this.red.setOpacity(0);
				var action = cc.MoveTo.create(0.2,cc.p( 220, 439));
				var action2 = cc.ScaleTo.create(0.2, 1, 1);
				this.runAction( action );
				this.runAction( action2 );
				this._lastStatus = this._status;			
			}
		}else if(this._status == 4 && this._lastStatus != this._status){
			if( _x != 265 || _y != 443){
				this.setOpacity(255);
				this.red.setOpacity(255);
				var action = cc.MoveTo.create(0.2,cc.p( 265, 440));
				var action2 = cc.ScaleTo.create(0.2, 1, 1);
				this.runAction( action );
				this.runAction( action2 );
				this._lastStatus = this._status;				
			}
		}
	},
	createWeapon: function(){
		this.initWithFile(s_icon_weapon_r[this._type-1]);
	},
	delay: function(){
		this.red = cc.ProgressTimer.create( cc.Sprite.create(s_icon_weapon[this._type - 1]) );
		this.red.setAnchorPoint( cc.p( 0,0));
        this.red.setType(cc.PROGRESS_TIMER_TYPE_RADIAL);
        this.addChild(this.red);
        //this.red.runAction(to1);
	}
});